package eu.smartx.learn.springjpa.repository;

import eu.smartx.learn.springjpa.entity.Address;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface AddressRepository extends Repository<Address, Long> {
    Address save(Address address);

    List<Address> findAll();

    void deleteAll();

    Address findById(Long id);
}

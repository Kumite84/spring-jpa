package eu.smartx.learn.springjpa;

import eu.smartx.learn.springjpa.entity.Address;
import eu.smartx.learn.springjpa.entity.Employee;
import eu.smartx.learn.springjpa.entity.Employer;
import eu.smartx.learn.springjpa.entity.Project;
import eu.smartx.learn.springjpa.repository.AddressRepository;
import eu.smartx.learn.springjpa.repository.EmployeeRepository;
import eu.smartx.learn.springjpa.repository.EmployerRepository;
import eu.smartx.learn.springjpa.repository.ProjectRepository;
import eu.smartx.learn.springjpa.service.AddressService;
import eu.smartx.learn.springjpa.service.EntityManagerService;
import org.hibernate.LazyInitializationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SpringJpaApplicationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AddressService addressService;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployerRepository employerRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private EntityManagerService entityManagerService;

    @Autowired
    EntityManager entityManager;

    @BeforeAll
    @Transactional
    public void beforeAll() {
        deleteData();
        createData();
    }

    @AfterAll
    public void afterAll() {
        deleteData();
    }

    /**
     * EntityManager ************************************
     */
    @Test
    void entityManagerException() {
        Assertions.assertThrows(LazyInitializationException.class,
                () -> {
                    List<Employee> result = entityManagerService.entityManagerException();
                    final String firstEmployeeName = result.get(0).getName();
                    Assertions.assertNotNull(firstEmployeeName);
                    getProjectsSizeForEmployeeList(result);
                });
    }

    @Test
    @Transactional
    void entityManagerNoExceptionTransactional() {
        List<Employee> result = entityManagerService.entityManagerException();
        getProjectsSizeForEmployeeList(result);
    }

    @Test
    void entityManagerNoExceptionJoinFetch() {
        List<Employee> result = entityManagerService.entityManagerNonTransactionalJoinFetch();
        getProjectsSizeForEmployeeList(result);
    }

    /**
     * CrudRepository ************************************
     */
    @Test
    void employeePersist() {
        Employee employee = new Employee();
        Employee persistedEmployee = employeeRepository.save(employee);
        Long id = persistedEmployee.getId();

        Assertions.assertTrue(employeeRepository.existsById(id));
    }

    @Test
    void employeeLoadAndUpdate() {
        Iterable<Employee> employeeIterable = employeeRepository.findAll();
        Iterator<Employee> it = employeeIterable.iterator();
        if (it.hasNext()) {
            Employee employee = it.next();

            final String newName = "name-" + new Date().getTime();
            employee.setName(newName);

            Employee modifiedEmployee = employeeRepository.save(employee);
            Optional<Employee> employeeOptional = employeeRepository.findById(modifiedEmployee.getId());

            if (employeeOptional.isPresent()) {
                Employee loadedModifiedEmployee = employeeOptional.get();
                Assertions.assertEquals(newName, loadedModifiedEmployee.getName());
            }
        }
    }

    @Test
    void employeePersistAndDeleteMultiple() {
        Employee persistedEmployee1 = employeeRepository.save(new Employee());
        Employee persistedEmployee2 = employeeRepository.save(new Employee());

        final List<Employee> employeeList = Arrays.asList(persistedEmployee1, persistedEmployee2);
        employeeRepository.deleteAll(employeeList);

        Assertions.assertFalse(employeeRepository.existsById(persistedEmployee1.getId()));
        Assertions.assertFalse(employeeRepository.existsById(persistedEmployee2.getId()));
    }

    @Test
    public void countEmployees() {
        Assertions.assertTrue(employeeRepository.count() > 0);
    }

    /**
     * PagingAndSortingRepository ************************************
     */
    @Test
    public void sortingEmployer() {
        Iterable<Employer> employerIterable = employerRepository.findAll(Sort.by("name").descending());

        List<Employer> repositoryOrderedEmployerList = new ArrayList<>();
        employerIterable.forEach(repositoryOrderedEmployerList::add);

        final Comparator<Employer> employerComparatorByName = Comparator.comparing(
                Employer::getName, Comparator.reverseOrder());
        final List<Employer> programOrderedEmployerList = repositoryOrderedEmployerList.stream().sorted(employerComparatorByName).collect(Collectors.toList());
        Assertions.assertEquals(repositoryOrderedEmployerList, programOrderedEmployerList);
    }

    @Test
    public void pagingEmployer() {
        PageRequest pageRequest13 = PageRequest.of(1, 3, Sort.by("id").ascending());
        Iterable<Employer> employerIterable13 = employerRepository.findAll(pageRequest13);
        Employer employer13 = employerIterable13.iterator().next(); // first

        PageRequest pageRequest31 = PageRequest.of(3, 1, Sort.by("id").ascending());
        Iterable<Employer> employerIterable31 = employerRepository.findAll(pageRequest31);
        Employer employer31 = employerIterable31.iterator().next(); // first

        Assertions.assertEquals(employer13.getId(), employer31.getId());
    }

    /**
     * Repository ************************************
     */
    @Test
    void addressException() {
        /**
         * https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.core-concepts
         */
        List<Address> addressList = addressRepository.findAll();
        Assertions.assertThrows(LazyInitializationException.class,
                () -> getProjectsSizeForAddressList(addressList));
    }

    @Test
    @Transactional
    void addressTransactional() {
        List<Address> addressList = addressRepository.findAll();
        getProjectsSizeForAddressList(addressList);
    }

    @Test
    void addressService() {
        int num = addressService.numOfProjectsForRandomAddress();
        Assertions.assertTrue(num > 0);
    }

    /**
     * Query Method ************************************
     * <p>
     * JpaRepository ************************************
     * The JpaRepository should be avoided if possible,
     * because it ties you repositories to the JPA persistence technology,
     * and in most cases you probably wouldn’t even use the extra methods provided by it.
     * <p>
     * The JpaRepository adds JPA-specific methods,
     * like flush() to trigger a flush on the persistence context
     * or findAll(Example<S> example) to find entities by example, to the PagingAndSortingRepository.
     */
    @Test
    @Transactional
    public void project() {
        // https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation
        // Table 3. Supported keywords inside method names

        Assertions.assertTrue(projectRepository.countByName("Project 01") > 0);

        Assertions.assertTrue(projectRepository.countByName("PROJECT 01") > 0); // ??? ! - MySQL!

        Assertions.assertTrue(projectRepository.countByNameIgnoreCase("PRojEcT 01") > 0); // ??? ! - MySQL!

        Iterable<Employee> employees = employeeRepository.findAll();
        List<Project> projectListAll = projectRepository.findByEmployeesIn(employees);

        Assertions.assertEquals(4, projectListAll.size()); // ???
        for (Project project : projectListAll) {
            System.out.println(project);
        }

        System.out.println("");

        List<Project> projectListDistinct = projectRepository.findDistinctByEmployeesIn(employees);
        Assertions.assertEquals(2, projectListDistinct.size());
        for (Project project : projectListDistinct) {
            System.out.println(project);
        }

        List<Project> projectListDistinctAndNameEndingWith = projectRepository.findDistinctByEmployeesInAndNameStartingWith(employees, "Project 02");
        Assertions.assertEquals(1, projectListDistinctAndNameEndingWith.size());

        // https://www.leafyjava.com/2018/10/spring-data-repository-query-precedence.html
        List<Project> projectListDistinctAndNameEndingWith3Ors
                = projectRepository.findDistinctByEmployeesInAndNameStartingWithOrNameStartingWithOrNameStartingWith(
                employees, "Project",
                "y",
                "A");
        Assertions.assertEquals(3, projectListDistinctAndNameEndingWith3Ors.size()); // ?????

        // https://www.leafyjava.com/2018/10/spring-data-repository-query-precedence.html
        List<Project> projectListDistinctAndNameEndingWith3OrsAndEmployees
                = projectRepository.findDistinctByEmployeesInAndNameStartingWithOrEmployeesInAndNameStartingWithOrEmployeesInAndNameStartingWith(
                employees, "Project",
                employees, "y",
                employees, "A");
        Assertions.assertEquals(2, projectListDistinctAndNameEndingWith3OrsAndEmployees.size()); // !!!

    }

    /**
     * @NamedQuery ************************************
     */
    @Test
    public void namedQuery() {
        long allEmployers = employerRepository.count();
        Employer lowestIdEmployer = employerRepository.findTopByOrderByIdAsc();
        List<Employer> filteredEmployers = entityManagerService.findAllEmployersOrderedByNameDescending(lowestIdEmployer.getId());
        Assertions.assertEquals(allEmployers - 1, filteredEmployers.size());

        final List<Employer> allOrderedByNameAscendingRandomBlaBlaBla = employerRepository.findAllOrderedByNameAscendingRandomBlaBlaBla();
        Assertions.assertTrue(allOrderedByNameAscendingRandomBlaBlaBla.size() > 0);
    }

    /**
     * @Query ************************************
     */
    @Test
    public void query() {
        Employer employer = employerRepository.findTopByOrderByIdAsc();

        Assertions.assertThrows(IncorrectResultSizeDataAccessException.class,
                () -> {
                    try {
                        employeeRepository.queryByEmployerId(employer.getId());
                    } catch (Exception e) {
                        e.printStackTrace(System.out);
                        throw e;
                    }
                });

        List<Employee> employeeList = employeeRepository.queryListByEmployerId(employer.getId());
        Assertions.assertTrue(employeeList.size() > 0);

        Employee employeeByEmployerName
                = employeeRepository.queryByEmployerName("Employer 02");
        Assertions.assertNotNull(employeeByEmployerName);

        Employee employeeByEmployerNameNoResult
                = employeeRepository.queryByEmployerName("NO RESULT");
        Assertions.assertNull(employeeByEmployerNameNoResult);

        Optional<Employee> employeeByEmployerNameOptional
                = employeeRepository.queryByEmployerNameOptional("Employer 02");
        Assertions.assertNotNull(employeeByEmployerNameOptional.get());

        Optional<Employee> employeeByEmployerNameOptionalNoResult
                = employeeRepository.queryByEmployerNameOptional("NO RESULT");
        Assertions.assertFalse(employeeByEmployerNameOptionalNoResult.isPresent());
    }

    @Test
    public void nativeQuery() {
        String s = employeeRepository.queryConcat();
        Assertions.assertTrue(s.startsWith("employee count=["));
    }

    /**
     * Stored Procedure ************************************
     */
    @Test
    public void storedProcedure() {
        /**
         DELIMITER //
         CREATE PROCEDURE GET_TOTAL_EMPLOYEES_BY_NAME(IN name_in VARCHAR(255), OUT count_out INT)
         BEGIN
         SELECT COUNT(*) into count_out FROM work.employee employee WHERE employee.name = name_in;
         END //
         DELIMITER ;
         */

        int count0 = employeeRepository.GET_TOTAL_EMPLOYEES_BY_NAME("zero");
        Assertions.assertEquals(0, count0);

        int count2 = employeeRepository.GET_TOTAL_EMPLOYEES_BY_NAME("Employee 22");
        Assertions.assertEquals(2, count2);
    }

    /**
     * Criteria API ************************************
     */
    @Test
    public void criteria() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
        Root<Employee> employeeRoot = criteriaQuery.from(Employee.class);
        criteriaQuery.select(employeeRoot);

        TypedQuery<Employee> query = entityManager.createQuery(criteriaQuery);
        List<Employee> employeeList = query.getResultList();
        Assertions.assertTrue(employeeList.size() > 0);

        ParameterExpression<String> nameParam = criteriaBuilder.parameter(String.class);
        criteriaQuery.where(criteriaBuilder.like(employeeRoot.get(Employee.NAME_FIELD), nameParam));
        TypedQuery<Employee> queryName1 = entityManager.createQuery(criteriaQuery);
        queryName1.setParameter(nameParam, "ZERO");
        List<Employee> employeeZeroList = queryName1.getResultList();
        Assertions.assertEquals(0, employeeZeroList.size());

        TypedQuery<Employee> queryName2 = entityManager.createQuery(criteriaQuery);
        queryName2.setParameter(nameParam, "%Employee 2%");
        List<Employee> employeeNotEmptyList = queryName2.getResultList();
        Assertions.assertTrue(employeeNotEmptyList.size() > 0);

        Assertions.assertThrows(LazyInitializationException.class,
                () -> getProjectsSizeForEmployeeList(employeeNotEmptyList));
    }

    @Test
    public void criteriaJoined() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Employee> criteriaQueryJoined = criteriaBuilder.createQuery(Employee.class);
        Root<Employee> employeeJoinedRoot = criteriaQueryJoined.from(Employee.class);
        employeeJoinedRoot.fetch("projects", JoinType.LEFT);
        List<Employee> employeeJoinedList = entityManager.createQuery(criteriaQueryJoined).getResultList();

        Assertions.assertDoesNotThrow(() -> employeeJoinedList.get(0).getProjects().size());
    }

    @Test
    @Transactional
    public void caching() {
        // watch Hibernate SQLs in logs
        System.out.println(" caching 01");
        Assertions.assertEquals(2, employeeRepository.findByNameIn(Arrays.asList("Employee 04", "Employee 05")).size());
        System.out.println(" caching 02");
        Assertions.assertEquals(2, employeeRepository.findByNameIn(Arrays.asList("Employee 04", "Employee 05")).size());
        System.out.println(" caching 03");
        Assertions.assertEquals(1, employeeRepository.findByNameIn(Arrays.asList("Employee 04")).size());
        System.out.println(" caching 04");
        employeeRepository.deleteByName("Employee 04");
        System.out.println(" caching 05");
        Assertions.assertEquals(null, employeeRepository.findByName("Employee 04"));
        System.out.println(" caching 06");
        Assertions.assertEquals(2, employeeRepository.findByNameIn(Arrays.asList("Employee 04", "Employee 05")).size()); // ??!!
        System.out.println(" caching 07");
        Assertions.assertEquals(1, employeeRepository.findByNameIn(Arrays.asList("Employee 04")).size()); // ??!
        System.out.println(" caching 08");
        employeeRepository.deleteByNameIn(Arrays.asList("Employee 04"));
        System.out.println(" caching 09");
        Assertions.assertEquals(1, employeeRepository.findByNameIn(Arrays.asList("Employee 04", "Employee 05")).size()); // !!
        System.out.println(" caching 10");
        Assertions.assertEquals(0, employeeRepository.findByNameIn(Arrays.asList("Employee 04")).size()); // !!!
        System.out.println(" caching 11");
        employeeRepository.deleteByNameIn(Arrays.asList("Employee 04", "Employee 05"));
        Assertions.assertEquals(0, employeeRepository.findByNameIn(Arrays.asList("Employee 04", "Employee 05")).size());
        System.out.println(" caching END");
    }

    @Test
    public void validateFlywayScript() {
        Employer employer = employerRepository.findById(1L).get();
        Assertions.assertEquals("SmartX Solutions Kft. FLYWAY", employer.getName());
    }

    @Test
    public void validateLiquibaseScript() {
        Employer employer = employerRepository.findById(1L).get();

        try {
            Assertions.assertEquals("SmartX Solutions Kft. LIQUIBASE", employer.getName());
        } finally {
            deleteData();
        }
    }

    @Test
    public void testRestDB() throws JSONException {
        String rootRestRp = this.restTemplate.getForObject("http://localhost:" + port + "/", String.class);
        printAsJson("rootRestRp", rootRestRp);

        String employeePathRestRp = this.restTemplate.getForObject("http://localhost:" + port + "/employee-path", String.class);
        printAsJson("employeePathRestRp", employeePathRestRp);

        Employee employee = this.restTemplate.getForObject("http://localhost:" + port + "/employee-path/search/findByName?name=Employee 01", Employee.class);
        System.out.println("*** Employee: " + employee);

        // You can also issue PUT, PATCH, and DELETE REST calls to replace, update, or delete existing records (respectively). The following example uses a PUT call:
    }

    private void printAsJson(String title, String restRp) throws JSONException {
        JSONObject json = new JSONObject(restRp);
        System.out.println("**********" + "\n"
                        + title + "\n"
                        + "**********" + "\n"
                        + json.toString(4)
                + "\n" + "\n" + "\n");
    }

    private void getProjectsSizeForEmployeeList(List<Employee> result) {
        result.stream()
                .forEach(employee -> {
                    employee.getProjects().size();
                });
    }

    private void getProjectsSizeForAddressList(List<Address> addressList) {
        addressList.stream()
                .forEach(address -> {
                    Employee employee = address.getEmployee();
                    getProjectsSizeForEmployeeList(Arrays.asList(employee));
                });
    }

    private Employer createAndSaveEmployer(String name) {
        Employer employer = new Employer();
        employer.setName(name);
        return employerRepository.save(employer);
    }

    private Employee createAndSaveEmployee(Employer employer, String name) {
        Employee employee = new Employee();
        employee.setName(name);
        employee.setEmployer(employer);

        return employeeRepository.save(employee);
    }

    private Address createAndSaveAddress(Employee employee1, String value) {
        Address address = new Address();
        address.setEmployee(employee1);
        address.setValue(value);

        return addressRepository.save(address);
    }

    private Project createAndSaveProject(String name, Employee... employees) {
        Project project = new Project();
        project.setName(name);

        if (employees != null) {
            project.setEmployees(new HashSet<>(Arrays.asList(employees)));
        }

        return projectRepository.save(project);
    }


    private void createData() {
        Employer employer1 = createAndSaveEmployer("Employer 01");
        Employer employer2 = createAndSaveEmployer("Employer 02");
        Employer employer3 = createAndSaveEmployer("Employer 03");
        Employer employer4 = createAndSaveEmployer("Employer 04");
        Employer employer5 = createAndSaveEmployer("Employer 05");
        Employer employer6 = createAndSaveEmployer("Employer 06");

        Employee employee1 = createAndSaveEmployee(employer1, "Employee 01");
        Employee employee2 = createAndSaveEmployee(employer1, "Employee 02");
        Employee employee3 = createAndSaveEmployee(employer1, "Employee 03");
        Employee employee4 = createAndSaveEmployee(employer2, "Employee 04");
        Employee employee5 = createAndSaveEmployee(employer3, "Employee 05");
        Employee employee6 = createAndSaveEmployee(employer3, "Employee 06");
        Employee employee_2_1 = createAndSaveEmployee(employer3, "Employee 22");
        Employee employee_2_2 = createAndSaveEmployee(employer3, "Employee 22");

        Address address1 = createAndSaveAddress(employee1, "Address 01");
        Address address2 = createAndSaveAddress(employee2, "Address 02");
        Address address3 = createAndSaveAddress(employee3, "Address 03");
        Address address4 = createAndSaveAddress(employee3, "Address 04");
        Address address5 = createAndSaveAddress(employee3, "Address 05");

        Project project1 = createAndSaveProject("Project 01", employee1, employee2, employee3);
        Project project2 = createAndSaveProject("Project 02", employee1);
        Project project3 = createAndSaveProject("X Project 03");
        Project project4 = createAndSaveProject("Y Project 04");

    }

    @Transactional
    private void deleteData() {
        addressRepository.deleteAll();
        projectRepository.deleteAll();
        employeeRepository.deleteAll();
        employerRepository.deleteAll();
    }
}

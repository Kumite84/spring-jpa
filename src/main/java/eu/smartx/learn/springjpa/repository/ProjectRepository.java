package eu.smartx.learn.springjpa.repository;

import eu.smartx.learn.springjpa.entity.Employee;
import eu.smartx.learn.springjpa.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Long> {

    int countByName(String name);

    int countByNameIgnoreCase(String name);

    @Query(value = "SELECT COUNT(*) FROM work.project p where p.name LIKE ':name'", nativeQuery = true)
    int queryCountByName(@Param("name") String name);

    List<Project> findByEmployeesIn(Iterable<Employee> employees);

    List<Project> findDistinctByEmployeesIn(Iterable<Employee> employees);

    List<Project> findDistinctByEmployeesInAndNameStartingWith(Iterable<Employee> employees, String namePrefix);

    List<Project> findDistinctByEmployeesInAndNameStartingWithOrNameStartingWithOrNameStartingWith(Iterable<Employee> employees, String namePrefix1,
                                                                                                   String namePrefix2,
                                                                                                   String namePrefix3);

    List<Project> findDistinctByEmployeesInAndNameStartingWithOrEmployeesInAndNameStartingWithOrEmployeesInAndNameStartingWith(Iterable<Employee> employees1, String namePrefix1,
                                                                                                                               Iterable<Employee> employees2, String namePrefix2,
                                                                                                                               Iterable<Employee> employees3, String namePrefix3);
}

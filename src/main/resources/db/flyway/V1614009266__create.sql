create table address (id bigint not null auto_increment, value varchar(255), employee_id bigint, primary key (id)) engine=InnoDB;
create table employee (id bigint not null auto_increment, name varchar(255), employer_id bigint, primary key (id)) engine=InnoDB;
create table employee_project (project_id bigint not null, employee_id bigint not null, primary key (project_id, employee_id)) engine=InnoDB;
create table employer (id bigint not null auto_increment, name varchar(255), primary key (id)) engine=InnoDB;
create table project (id bigint not null auto_increment, name varchar(255), primary key (id)) engine=InnoDB;

create table run_count (id bigint not null auto_increment, run_count bigint, primary key (id)) engine=InnoDB;

alter table address add constraint FKq4m60pqp7shng4u5n9h2346mp foreign key (employee_id) references employee (id);
alter table employee add constraint FKrmknjkdeoo3molal1gxkt9dar foreign key (employer_id) references employer (id);
alter table employee_project add constraint FKb25s5hgggo6k4au4sye7teb3a foreign key (employee_id) references employee (id);
alter table employee_project add constraint FK4yddvnm7283a40plkcti66wv9 foreign key (project_id) references project (id);
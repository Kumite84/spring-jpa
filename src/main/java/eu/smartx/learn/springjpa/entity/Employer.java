package eu.smartx.learn.springjpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "employer")
@NamedQuery(name = "Employer.findAllOrderedByNameDescending",
        query = "SELECT employer FROM Employer employer WHERE employer.id > :id ORDER BY employer.name DESC")
@NamedQuery(name = "Employer.findAllOrderedByNameAscendingRandomBlaBlaBla",
        query = "SELECT employer FROM Employer employer ORDER BY employer.name ASC")
public class Employer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override public String toString() {
        return "Employer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

package eu.smartx.learn.springjpa.repository;

import eu.smartx.learn.springjpa.entity.Employee;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(path = "employee-path")
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Employee findByName(@Param("name") String name);

    @Query(value = "Select employee FROM Employee employee where employee.employer.id = :employerId")
    Employee queryByEmployerId(@Param("employerId") Long employerId);

    @Query(value = "Select employee FROM Employee employee where employee.employer.id = :employerId")
    List<Employee> queryListByEmployerId(@Param("employerId") Long employerId);

    @Query(value = "Select employee FROM Employee employee where employee.employer.name = :employerName")
    Employee queryByEmployerName(@Param("employerName") String employerName);

    @Query(value = "Select employee FROM Employee employee where employee.employer.name = :employerName")
    Optional<Employee> queryByEmployerNameOptional(@Param("employerName") String employerName);

    @Query(value = " SELECT CONCAT(\"employee count=[\", COUNT(*), \"]\") FROM work.employee",
            nativeQuery = true)
    String queryConcat();

    @Procedure
    int GET_TOTAL_EMPLOYEES_BY_NAME(String name);

    @Procedure
    int NO_PROC();

    @Cacheable("namesIn") // Spring annotation!
    List<Employee> findByNameIn(List<String> names);

    Long deleteByName(String name);

    @Caching(evict = {
            @CacheEvict(value = "namesIn", allEntries = true)
    })
    Long deleteByNameIn(List<String> names);

}

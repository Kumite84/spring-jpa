package eu.smartx.learn.springjpa.service;

import eu.smartx.learn.springjpa.entity.Employee;
import eu.smartx.learn.springjpa.entity.Employer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class EntityManagerService {

    @Autowired
    EntityManager entityManager;

    public List<Employee> entityManagerException() {
        TypedQuery<Employee> query =
                entityManager.createQuery("SELECT employee FROM Employee employee " +
                        "WHERE employee.employer.name=:employerName", Employee.class);
        query.setParameter("employerName", "Employer 02");
        List<Employee> result = query.getResultList();

        return result;
    }

    public List<Employee> entityManagerNonTransactionalJoinFetch() {
        TypedQuery<Employee> query =
                entityManager.createQuery("SELECT employee FROM Employee employee JOIN FETCH employee.projects projects " +
                        "WHERE employee.employer.name=:employerName", Employee.class);
        query.setParameter("employerName", "Employer 02");
        List<Employee> result = query.getResultList();

        return result;
    }

    public List<Employer> findAllEmployersOrderedByNameDescending(Long idGreaterThanFilter) {
        Query q = entityManager.createNamedQuery("Employer.findAllOrderedByNameDescending", Employer.class);
        q.setParameter("id", idGreaterThanFilter);
        List<Employer> employerList = q.getResultList();

        return employerList;
    }

}
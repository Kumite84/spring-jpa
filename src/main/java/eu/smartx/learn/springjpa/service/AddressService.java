package eu.smartx.learn.springjpa.service;

import eu.smartx.learn.springjpa.entity.Address;
import eu.smartx.learn.springjpa.entity.Employee;
import eu.smartx.learn.springjpa.entity.Project;
import eu.smartx.learn.springjpa.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    public int numOfProjectsForRandomAddress() { // class is @Transactional
        List<Address> addressList = addressRepository.findAll();
        Address address = addressList.stream().findAny().get();
        Employee employee = address.getEmployee();
        Set<Project> projects = employee.getProjects();
        return projects.size();
    }

}

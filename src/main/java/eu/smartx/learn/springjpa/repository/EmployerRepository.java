package eu.smartx.learn.springjpa.repository;

import eu.smartx.learn.springjpa.entity.Employer;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Collection;
import java.util.List;

public interface EmployerRepository extends PagingAndSortingRepository<Employer, Long> {

    Employer findTopByOrderByIdAsc();

    List<Employer> findAllOrderedByNameAscendingRandomBlaBlaBla();

    void deleteByIdNotIn(Collection<Long> idsNotToDelete);
}
